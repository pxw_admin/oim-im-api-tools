package com.oim.core.business.back;

import com.onlyxiahui.net.data.action.DataBackActionAdapter;

/**
 * @author: XiaHui
 * @date: 2018-02-22 13:34:17
 */
public abstract class DataBackActionImpl extends DataBackActionAdapter{

	
	public abstract void back(String message);
}
